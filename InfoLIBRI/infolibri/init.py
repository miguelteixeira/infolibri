# -*- coding: utf-8 -*-
from flask import Flask, escape, request, render_template, jsonify
from os import sys
sys.path.insert(0, "/home/InfoLIBRI/infolibri/functions")
from functions import *
import isbnlib
import json
from dicttoxml import dicttoxml
from xml.dom.minidom import parseString


InfoLIBRI = Flask(__name__)

if __name__ == '__main__':
    InfoLIBRI.run(debug=True, host='0.0.0.0')

@InfoLIBRI.route('/', methods=['GET', 'POST'])
def FormInicio():
    if(request.method == "get"):
        return render_template('index.html', title = "InfoLIBRI")
    else:
        return render_template('index.html', title = "InfoLIBRI")


@InfoLIBRI.route('/Resultado', methods=['POST'])
def Resultado():
    if request.method == 'POST':
        form_pais = request.form['pais']
        form_busca_por = request.form['busca_por']
        form_dados = request.form['dados'].strip()
        print(form_dados)
        print(form_pais)
        if(form_busca_por == "ISBN"):
            result = ProcessaDados(form_dados, form_pais)
            if(result != 0):
                ##'''BRASIL'''##
                if(form_pais == "Brasil"):
                    try:
                        return render_template('BRASIL/resultadosBRISBN.html', title = "InfoLIBRI",
                                               titulo = result[1]['Título'],
                                               isbn   = result[1]['ISBN'],
                                               serie  = result[1]['Série'],
                                               colecao = result[1]['Coleção'],
                                               volume = result[1]['Volume'],
                                               edicao = result[1]['Edição'],
                                               tipo_de_suporte = result[1]["Tipo de Suporte"],
                                               paginas = result[1]['Páginas'],
                                               editor_a = result[1]['Editor(a)'],
                                               participacoes = result[1]['Participações'],
                                               ano_edicao = result[1]["Ano da Edição"],
                                               status_dados = result[0],
                                               pais = form_pais)
                    except:
                        return render_template('BRASIL/PaginaFIM.html', title = "InfoLIBRI",
                                fim = "O ISBN digitado é válido, mas talvez ainda não tenha sido cadastrado")
                
                elif(form_pais == "Google"):
                    if(type(result) == str):
                        return render_template('GoogleBooks/PaginaFIM.html', title = "InfoLIBRI",
                                fim = "Desculpe, o ISBN não foi encontrado :(")
                    else:
                        return render_template('GoogleBooks/ResultadosGoogleISBN.html', title = "InfoLIBRI",
                                               pais = form_pais,
                                               resultGoogle = result,
                                               status_dados = "Ainda não conectado a banco de dados")
                        
                        
            elif(result == 0):
                return render_template('BRASIL/PaginaFIM.html', title = "InfoLIBRI",
                                fim = "Desculpe, pode tentar novamente? Parece que você não digitou um ISBN válido :(")



        elif(form_busca_por == "titulo"):
            if(form_pais == "Brasil"):
                results = pesquisaTituloMongo(form_dados, form_pais)
                if(results == 0):
                    return render_template('BRASIL/PaginaFIM.html', title = "InfoLIBRI",
                                fim = "Desculpe, nenhum livro encontrado :(")

                else:
                    return render_template('BRASIL/resultadosBRTITULO.html', title = "InfoLIBRI",
                                           resultInicial = results[0],
                                           resultFinal = results[1],
                                           status_dados = "Dados importados!",
                                           pais = form_pais)
            elif(form_pais == "Google"):
                result = buscaGoogleBooks(form_dados, "Título")
                if(type(result) == str):
                    return render_template('GoogleBooks/PaginaFIM.html', title = "InfoLIBRI",
                                fim = "Desculpe, o Título não foi encontrado :(")
                else:
                    return render_template('GoogleBooks/ResultadosGoogleISBN.html', title = "InfoLIBRI",
                                               pais = form_pais,
                                               resultGoogle = result,
                                               status_dados = "Ainda não conectado a banco de dados")
                    

@InfoLIBRI.route('/ImportarDados', methods=['POST'])
def ImportarDados():
    if request.method == 'POST':
        try:
            form_pais_isbn = request.form["isbn-insert"]
            if("Brasil" in form_pais_isbn):
                form_isbn = form_pais_isbn.replace("Brasil", '')
                form_isbn = form_isbn.strip()
                importarDadosParaBD("Brasil", form_isbn)
                return render_template('BRASIL/PaginaFIM.html', title = "InfoLIBRI",
                                fim = "Obrigado por contribuir! O livro foi adicionado no banco de dados com sucesso!")
        except:
            return render_template('BRASIL/PaginaFIM.html', title = "InfoLIBRI",
                                fim = "Desculpe, algo de errado ocorreu na inserção do livro no banco de dados :(")
    return 0
        

@InfoLIBRI.route('/JSON', methods=['POST'])
def JSON():
    if request.method == 'POST':
        form_isbn_pais = request.form["json_isbn_pais"]
        if("Brasil" in form_isbn_pais):
            try:
                isbn = form_isbn_pais.replace("Brasil", '')
                isbn = isbn.strip()
                JSON = ProcessaDados(isbn, "Brasil")[1]
                dictFinal = {}
                for chave in JSON.keys():
                    if(chave != "_id"):
                        dictFinal[chave] = JSON[chave]
                JSONSTR = "<textarea readonly rows='40' cols='100'>" + json.dumps(dictFinal, ensure_ascii=False, indent=4) + "</textarea>"
                return JSONSTR
            except:
                return render_template('BRASIL/PaginaFIM.html', title = "InfoLIBRI",
                                    fim = "Desculpe, algo de errado ocorreu na geração do JSON :(")
        elif("Google" in form_isbn_pais):
            try:
                isbn = form_isbn_pais.replace("Google", '')
                isbn = isbn.strip()
                JSON = ProcessaDados(isbn, "Google")[0]
                JSONSTR = "<textarea readonly rows='40' cols='100'>" + json.dumps(JSON, ensure_ascii=False, indent=4) + "</textarea>"
                return JSONSTR
            except:
                return render_template('BRASIL/PaginaFIM.html', title = "InfoLIBRI",
                                    fim = "Desculpe, algo de errado ocorreu na geração do JSON :(")
    else:
        return render_template('BRASIL/PaginaFIM.html', title = "InfoLIBRI",
                                    fim = "Desculpe, algo de errado ocorreu na geração do JSON :(")



@InfoLIBRI.route('/XML', methods=['POST'])
def XML():
    if request.method == 'POST':
        form_isbn_pais = request.form["xml_isbn_pais"]
        if("Brasil" in form_isbn_pais):
            try:
                isbn = form_isbn_pais.replace("Brasil", '')
                isbn = isbn.strip()
                JSON = ProcessaDados(isbn, "Brasil")[1]
                dictFinal = {}
                for chave in JSON.keys():
                    if(chave != "_id"):
                        dictFinal[chave] = JSON[chave]
                xml = dicttoxml(dictFinal, custom_root='InfoLIBRI', attr_type=False)
                xmlDom = parseString(xml)
                xmlDom = xmlDom.toprettyxml()
                xmlDom = "<textarea readonly rows='40' cols='100'>" + xmlDom + "</textarea>"
                return xmlDom
            except:
                return render_template('BRASIL/PaginaFIM.html', title = "InfoLIBRI",
                                    fim = "Desculpe, algo de errado ocorreu na geração do XML :(")
        elif("Google" in form_isbn_pais):
            try:
                isbn = form_isbn_pais.replace("Google", '')
                isbn = isbn.strip()
                JSON = ProcessaDados(isbn, "Google")[0]
                xml = dicttoxml(JSON, custom_root='InfoLIBRI', attr_type=False)
                xmlDom = parseString(xml)
                xmlDom = xmlDom.toprettyxml()
                xmlDom = "<textarea readonly rows='40' cols='100'>" + xmlDom + "</textarea>"
                return xmlDom
            except:
                return render_template('BRASIL/PaginaFIM.html', title = "InfoLIBRI",
                                    fim = "Desculpe, algo de errado ocorreu na geração do JSON :(")
    else:
        return render_template('BRASIL/PaginaFIM.html', title = "InfoLIBRI",
                                    fim = "Desculpe, algo de errado ocorreu na geração do JSON :(")
