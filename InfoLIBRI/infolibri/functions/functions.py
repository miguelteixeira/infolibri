# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
from requests_html import HTMLSession
from lxml import etree, html
from fuzzywuzzy import fuzz
from isbnlib import *
import pymongo
from pymongo import MongoClient
import re


InfoLIBRIClient = MongoClient()
InfoLIBRIClient = MongoClient('localhost', 27017)

#---Banco de dados Brasil---#
InfoLIBRIdbBR = InfoLIBRIClient.InfoLibriBR

def retornaDictISBN(ISBN):

    #Obtendo dados da Biblioteca Nacional
    endPrefixo = "http://www.isbn.bn.br"
    endSufixo = etree.XPath('string(//img[@id="idImgCaptcha"]/@src)') #XPath do captcha extraído da página da Biblioteca Nacional
    
    session = HTMLSession()
    BNSiteComForm = session.get("http://www.isbn.bn.br/website/consulta/cadastro")
    codigoHTMLBN = BNSiteComForm.text
    root = etree.HTML(codigoHTMLBN)
    result = etree.tostring(root, pretty_print=True, method="html")
    result2 = endSufixo(root)
    enderecoCaptcha = endPrefixo + result2
    response = session.get(enderecoCaptcha)

    
    enderecoBNISBN = "http://www.isbn.bn.br/website/consulta/cadastro/isbn/" + str(ISBN)
    BNSiteDados = session.get(enderecoBNISBN)
    htmlCodigoFinal = BNSiteDados.text
    soup = BeautifulSoup(htmlCodigoFinal, 'html.parser')
    textoISBN = soup.get_text()
    listaSoup = textoISBN.split("\n")

    #Variável que guardará dados do ISBN - Sendo uma lista de Dicionários
    listaISBN = []
            
    dictISBN = {}
    for linha in listaSoup:
        if(len(linha.strip(" ")) > 1 and "inner" not in linha):
            if("ISBN" in linha and "(Apenas números)" not in linha):
                if(len(linha.strip()) == 22 or len(linha.strip()) == 18):
                    ISBN = linha.replace("ISBN", '')
                    ISBN = ISBN.replace("-", '')
                    ISBN = ISBN.replace("\n", '')
                    dictISBN["ISBN"] = ISBN.strip()
            elif("Título" in linha and "Título da Obra" not in linha):
                TITULO = linha.replace("Título", '')
                TITULO = TITULO.replace("\n", '')
                dictISBN["Título"] = TITULO.strip()
            elif("Nome Série" in linha and len(linha.strip(" ")) < 50):
                SERIE = linha.replace("Nome Série", '')
                SERIE = SERIE.replace("\n", '')
                dictISBN["Série"] = SERIE.strip()
            elif("Nome Coleção" in linha and len(linha.strip(" ")) < 50):
                COLECAO = linha.replace("Nome Coleção", '')
                COLECAO = COLECAO.replace("\n", '')
                dictISBN["Coleção"] = COLECAO.strip()
            elif("Volume" in linha and len(linha.strip(" ")) < 50):
                VOLUME = linha.replace("Volume", '')
                VOLUME = VOLUME.replace("\n", '')
                dictISBN["Volume"] = VOLUME.strip()
            elif('Edição' in linha and len(linha.strip(" ")) < 30 and "Ano" not in linha):
                EDICAO = linha.replace("Edição", '')
                EDICAO = EDICAO.replace("\n", '')
                dictISBN["Edição"] = EDICAO.strip()
            elif("Ano Edição" in linha and len(linha.strip(" ")) < 17):
                ANOEDICAO = linha.replace("Ano Edição", '')
                ANOEDICAO = ANOEDICAO.replace("\n", '')
                ANOEDICAO = ANOEDICAO.replace("Ano", '')
                dictISBN["Ano da Edição"] = ANOEDICAO.strip()
            elif("Tipo de Suporte" in linha and len(linha.strip(" ")) < 50):
                TIPOSUPORTE = linha.replace("Tipo de Suporte", '')
                dictISBN["Tipo de Suporte"] = TIPOSUPORTE.strip()
            elif("Páginas" in linha and len(linha.strip(" ")) < 20):
                PAGINAS = linha.replace("Páginas", '')
                PAGINAS = PAGINAS.replace(" ", '')
                PAGINAS = PAGINAS.replace("\n", '')
                dictISBN["Páginas"] = PAGINAS
            elif("Editor(a)" in linha and len(linha.replace(" ", "")) < 20 ):
                    contador = 0
                    indiceEdit = 0
                    indicePart = 0
                    indiceTrocaLabel = 0
                    while(contador < len(listaSoup)):
                        if("Editor(a)" in listaSoup[contador] and len(linha.strip(" ")) < 20):
                            indiceEdit = contador
                        contador+=1

                    contador = 0
                    while(contador < len(listaSoup)):
                        if("Participações" in listaSoup[contador] and len(linha.strip(" ")) < 20):
                            indicePart = contador
                        contador+=1

                    contador = indicePart
                    while(contador < len(listaSoup)):
                        if('function trocaLabel' in listaSoup[contador]):
                            indiceTrocaLabel = contador
                        contador += 1

                    indiceEdit+=1
                    for i in range(indiceEdit, indicePart):
                        if(len(listaSoup[i].replace(" ", '')) > 2):
                            EdTemp = listaSoup[i].strip()
                            EdTemp = EdTemp.replace("\t", '')
                            EdTemp = EdTemp.replace("\r", '')
                            dictISBN["Editor(a)"] = EdTemp
                    indicePart+=1

                    dictISBN["Participações"] = []
                    for j in range(indicePart, indiceTrocaLabel):
                        if(len(listaSoup[j].replace(" ", '')) > 2):
                            PartTemp = listaSoup[j].strip()
                            PartTemp = PartTemp.replace("\t", '')
                            PartTemp = PartTemp.replace("\r", '')
                            dictISBN["Participações"] += [PartTemp]

                                    

    if("ISBN" in dictISBN.keys() and dictISBN["ISBN"] == ""):
        return 0
    elif("ISBN" not in dictISBN.keys()):
        return 0
    else:
        if("Editor(a)" not in dictISBN.keys()):
            dictISBN["Editor(a)"] = " ";
        if("Série" not in dictISBN.keys()):
            dictISBN["Série"] = " "
        if("Coleção" not in dictISBN.keys()):
            dictISBN["Coleção"] = " "
        if("Páginas" not in dictISBN.keys()):
            dictISBN["Páginas"] == " "
        if("Título" not in dictISBN.keys()):
            dictISBN["Título"] = " "
        if("Volume" not in dictISBN.keys()):
            dictISBN["Volume"] = " "
        if("Edição" not in dictISBN.keys()):
            dictISBN["Edição"] = " "
        if("Ano da Edição" not in dictISBN.keys()):
            dictISBN["Ano da Edição"] = " "
        if("Tipo de Suporte" not in dictISBN.keys()):
            dictISBN["Tipo de Suporte"] = " "
        return dictISBN

def limpaEVerificaISBN(ISBN_FORM):
    ISBN = ISBN_FORM.strip()
    tamanho = len(ISBN)
    if(tamanho == 10 and is_isbn10(ISBN)):
        return True
    elif(tamanho == 13 and is_isbn13(ISBN)):
        return True
    else:
        return False

def buscaGoogleBooks(DADO, OPCAO):
    dadoLimpo = DADO.strip()
    if(OPCAO == "ISBN"):
        if(is_isbn13(dadoLimpo) or is_isbn10(dadoLimpo)):
            try:
                lista = goom(dadoLimpo)
                return lista
            except:
                return "ISBN não encontrado :("
    elif(OPCAO == "Título"):
        try:
            lista = goom(dadoLimpo)
            return lista
        except:
            return "Título não encontrado"

def ProcessaDados(ISBN_FORM, PAIS_FORM):
    if(limpaEVerificaISBN(ISBN_FORM)):
        if(PAIS_FORM == "Brasil"):
            try:
                result = ["Dados importados!"]
                result += [InfoLIBRIdbBR.LivrosBR.find({'ISBN': ISBN_FORM})[0]]
                return result
            except:
                result = ["Importar dados"]
                result += [retornaDictISBN(ISBN_FORM)]
                return result
        elif(PAIS_FORM == "Google"):
            try:
                return buscaGoogleBooks(ISBN_FORM, "ISBN")
            except:
                return 0
    else:
        return 0

def importarDadosParaBD(PAIS_FORM, DADO):
    if(PAIS_FORM == "Brasil"):
        try:
            documentDict = retornaDictISBN(DADO)
            InfoLIBRIdbBR.LivrosBR.insert_one(documentDict)
            return "Obrigado por contribuir! O livro foi adicionado no banco de dados com sucesso!"
        except:
            return "Desculpe, algo de errado ocorreu na inserção do livro no banco de dados :("
    return 0

def pesquisaTituloMongo(TITULO_FORM, PAIS_FORM):
    listaResultsInicial = []
    listaResultsFinal = []
    if(PAIS_FORM == "Brasil"):
        tituloRegex = re.compile(TITULO_FORM, re.IGNORECASE)
        consultaInicial = InfoLIBRIdbBR.LivrosBR.find({"Título": tituloRegex}).limit(100)
        contador = 0
        while(contador < consultaInicial.count()):
            listaResultsInicial += [consultaInicial[contador]]
            contador+=1
        #consulta um pouco mais detalhada
        listaRgx = []
        listaCandidRgx = TITULO_FORM.split()
        contador = 0
        
        while(contador < len(listaCandidRgx)):
            if(contador == len(listaCandidRgx) - 1):
                if(len(listaCandidRgx[contador])> 6):
                    tempRgx = listaCandidRgx[contador-1]+ " " + listaCandidRgx[contador]
                    listaRgx += [tempRgx]
            elif(len(listaCandidRgx[contador]) > 1 and len(listaCandidRgx[contador+1]) > 6 and contador != len(listaCandidRgx) - 2):
                tempRgx = listaCandidRgx[contador] + " " + listaCandidRgx[contador+1]
                listaRgx += [tempRgx]
            contador+=1
        contador = 0;
        
        while(contador < len(listaRgx)):
            listaRgx[contador] = re.compile(listaRgx[contador], re.IGNORECASE)
            contador += 1
            
        for element in listaRgx:
            consultaTemp = InfoLIBRIdbBR.LivrosBR.find({"Título": element}).limit(15)
            contador = 0
            while(contador < consultaTemp.count()):
                listaResultsFinal += [contultaTemp[contador]]
                contador+=1

        resultsGeral = []
        resultsGeral += [listaResultsInicial]
        resultsGeral += [listaResultsFinal]

        if(len(listaResultsInicial) == 0 and len(listaResultsFinal) == 0):
            return 0;

        return resultsGeral
    else:
        return "ainda nao implementado"


