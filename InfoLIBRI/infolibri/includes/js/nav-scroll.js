// Rolagem de 150px percebida na pagina, surge o botao scrollup
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 150) {
    document.getElementById("scroll-top").style.display = "block";
  } else {
    document.getElementById("scroll-top").style.display = "none";
  }
}

// Executa a ação quando clicado
function topFunction() {

  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0; 

  
} 