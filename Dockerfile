FROM ubuntu:18.04
LABEL maintainer="Miguel Teixeira"
RUN apt update && apt upgrade -y
CMD locale-gen pt_BR.UTF-8\
    dpkg-reconfigure locales\
    export LC_ALL=pt_BR.UTF-8\
    export LANG=pt_BR.UTF-8\
    apt update\
RUN apt install -y build-essential
RUN apt install -y python3
RUN apt install -y python3-pip
RUN apt install python3-dev
RUN apt install -y mongodb
RUN pip3 install flask
RUN pip3 install isbnlib
RUN pip3 install dicttoxml
RUN pip3 install requests_html
RUN pip3 install lxml
RUN pip3 install fuzzywuzzy
RUN pip3 install pymongo
RUN pip3 install beautifulsoup4
RUN pip3 install pip-tools


RUN apt install mongo-tools -y
CMD mongo
CMD use InfoLIBRIdbBR
CMD exit
COPY InfoLIBRI /home/InfoLIBRI

RUN service mongodb start\
    mongoimport --db InfoLibriBR --collection LivrosBR --drop --file /home/InfoLIBRI/BD/LivrosBR.json

RUN chown -R mongodb:mongodb /var/lib/mongodb
CMD mongo
CMD use InfoLIBRIdbBR
CMD InfoLIBRIdbBR.repairDatabase()
CMD exit

CMD ufw allow 27017
CMD service mongodb restart

CMD service mongodb start --bind_ip=0.0.0.0 --port 27017 && FLASK_APP=/home/InfoLIBRI/infolibri/init.py LC_ALL=C.UTF-8 LANG=C.UTF-8 flask run --host=0.0.0.0 


EXPOSE 5000 27017




