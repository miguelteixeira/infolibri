# InfoLIBRI

###### - Web-app para importação de metadados associados ao ISBN (Uma melhor descrição será adicionada posteriormente. 

###### * Busca metadados associados à Biblioteca Nacional (Agencia Nacional do ISBN) e portais oficiais de outros países. 
###### - Este é um projeto de Web Mining e Scraping


##### Para inciciar a aplicação:

..* docker build -t infolibri .
..* docker container run --name infoTesteAplicattion -ti -p 5000:5000 -p 27017:27017  infolibri

..* Acesse a página da InfoLibri pelo navegador no seguinte endereço:
..* http://0.0.0.0:5000


